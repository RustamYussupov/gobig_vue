export const state = () => ({
    development: '',
    marketing: '',
    promotion: '',
    recommendation: '',
    loaded: false
});

export const mutations = {
    SET_INDEX_PAGE_DATA_TO_STATE (state, data) {
        state.development = data.development;
        state.marketing = data.marketing;
        state.promotion = data.promotion;
        state.recommendation = data.recommendation;
        state.loaded = true;
    }
};

export const actions = {
    async GET_INDEX_PAGE_DATA_FROM_API ({commit}) {
        try {
            const development = await this.$axios.$get(`${this.$axios.defaults.baseURL}/development`);
            const marketing = await this.$axios.$get(`${this.$axios.defaults.baseURL}/marketing`);
            const promotion = await this.$axios.$get(`${this.$axios.defaults.baseURL}/promotion`);
            const recommendation = await this.$axios.$get(`${this.$axios.defaults.baseURL}/recommendation`);
            commit('SET_INDEX_PAGE_DATA_TO_STATE', {development, marketing, promotion, recommendation});
        } catch (e) {
            throw e;
        }
    }
};

export const getters = {
    DEVELOPMENT: (state) => state.development,
    MARKETING: (state) => state.marketing,
    PROMOTION: (state) => state.promotion,
    RECOMMENDATION: (state) => state.recommendation,
    LOADED: (state) => state.loaded
};