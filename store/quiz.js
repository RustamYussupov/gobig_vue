export const state = () => ({
    quiz: {}
});

export const mutations = {
    SET_QUIZ_PAGE_DATA_TO_STATE(state, data) {
        state.quiz = data;
    }
};

export const actions = {
    async GET_QUIZ_PAGE_DATA_FROM_API({commit}) {
        try {
            const quiz = await this.$axios.$get(`${this.$axios.defaults.baseURL}/quiz`);
            commit('SET_QUIZ_PAGE_DATA_TO_STATE', quiz)
        } catch (e) {
            throw(e)
        }
    }
};

export const getters = {
    QUIZ: (state) => state.quiz
};