export const state = () => ({
    contacts: {}
});

export const mutations = {
    SET_CONTACTS_TO_STATE (state, contacts) {
        state.contacts = contacts;
    }
};

export const actions = {
    async GET_CONTACTS_FROM_API ({commit}) {
        try {
            const contacts = await this.$axios.$get(`${this.$axios.defaults.baseURL}/contacts`);
            commit('SET_CONTACTS_TO_STATE', contacts);
        } catch (e) {
            throw e;
        }
    }
};

export const getters = {
    CONTACTS: (state) => state.contacts
};
