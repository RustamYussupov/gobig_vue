export default {
    mode: 'universal',

    target: 'server',

    head: {
        title: 'goBIG.kz',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: ''}
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
        ]
    },

    css: [
        'swiper/swiper.scss',
        'vue-select/dist/vue-select.css',
        '~assets/scss/main.scss'
    ],

    loading: {
        color: '#ff4477'
    },

    styleResources: {
        scss: ['assets/scss/variables/variables.scss']
    },

    plugins: [
        '~plugins/vue-scrollto',
        '~plugins/vue-select',
        '~plugins/vuelidate',
        { src: '~plugins/vue-swiper.js', ssr: false },
    ],

    components: true,

    buildModules: [
        // Doc: https://github.com/nuxt-community/eslint-module
        '@nuxtjs/eslint-module'
    ],

    modules: [
        // Doc: https://axios.nuxtjs.org/usage
        '@nuxtjs/axios',
        '@nuxtjs/style-resources'
    ],

    axios: {
        baseURL:"http://localhost:3004"
    },

    build: {
        vendor: [
            'vue-scrollto',
            'vue-select',
            'vue-swiper',
            'vuelidate'
        ]
    }
};
